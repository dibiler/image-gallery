@extends('layouts.app')

@section('content')
<div class="m-10 p-3 bg-white rounded shadow-sm min-h-screen">
    <h1>Welcome to your content, please select an element on the list to view the available content:</h1>
    <div class="container" x-data="{
        group: {
            {{ $models->map(function($item,$index) use ($active) { return "'".$item."':".(strtolower($item)==$active? 'true':'false');})->join(',') }}
        },
            showItems(enable){ for (let item in this.group) { this.group[item]=false; } console.log(enable); console.log(this.group); this.group[enable]=true; } 
    }">
        <div class="well">
            @foreach ($models as $model)
            <h3><a href="#" @click.prevent="showItems('{{ $model }}')" :class="{'text-blue-500': group.{{ $model }} ===true }">{{ $model }}</a></h3>
            @endforeach
        </div>
        <div class="content flex flex-wrap">
            @foreach ($elements as $element=>$items)
                @foreach ($items as $item)
                    @switch($element)
                        @case('Cars')
                            <a href="{{ route('cars.gallery', $item->id) }}" class="m-2 p-2 bg-slate-400 w-1/6 flex flex-col text-white rounded" x-show="group.{{ $element }}">
                                <p class="w-1/2 text-2xl">{{ $item->model }}</p>
                                <p class="w-1/2">{{ $item->brand }}</p>
                            </a>
                        @break
                        @case('Posts')
                            <a href="{{ route('posts.gallery', $item->id) }}" class="m-2 p-2 bg-slate-400 w-1/6 flex flex-col text-white rounded min-h-fit h-16" x-show="group.{{ $element }}">
                                <p class="w-full">{{ $item->title }}</p>
                            </a>
                        @break
                        @case('Authors')
                            <a href="{{ route('authors.gallery', $item->id) }}" class="m-2 p-2 bg-slate-400 w-1/6 flex flex-col text-white rounded" x-show="group.{{ $element }}">
                                <p class="w-full">{{ $item->name }}</p>
                                <p class="w-1/2">{{ $item->age }}</p>
                            </a>
                        @break
                        @case('Locations')
                            <a href="{{ route('locations.gallery', $item->id) }}" class="m-2 p-2 bg-slate-400 w-1/6 flex flex-col text-white rounded" x-show="group.{{ $element }}">
                                <p class="w-full">{{ $item->name }}</p>
                            </a>
                        @break
                    @endswitch
                @endforeach
            @endforeach
        </div>
    </div>
</div>
@endsection
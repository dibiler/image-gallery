@extends('layouts.app')

@section('content')
    <div class="flex m-auto justify-center h-screen items-center">
        Sorry. Location does not have a gallery yet.
    </div>
@endsection
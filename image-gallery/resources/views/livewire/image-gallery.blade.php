<div>
    <div class="p-20 h-screen max-h-screen from-slate-300 to-black bg-gradient-to-b grid grid-rows-8 grid-flow-row gap-4" x-data>
        <!-- First row -->
        <div class="flex flex-row justify-between items-center row-span-1">
            <h1 class="text-white">Image Gallery for {{ $instanceName }}: {{ $instance->getName() }}</h1>
            <a href="{{ route('home',['group'=>strtolower($pluralInstanceName)]) }}" class="text-white text-sm flex flex-row justify-start items-center" ><x-left class=" drop-shadow bg-blue-500 h-8 w-8 flex rounded-full justify-content-center align-items-center mr-3"></x-left> <span class="hover:underline">Back to {{ $pluralInstanceName }} List</span></a>
        </div>
        <!-- Second row -->
        @if (sizeof($instance->images)==0)
            <div class="flex m-auto justify-center h-30 items-center row-span-5 text-white w-full">
                Gallery does not have images yet.
            </div>
        @else
        @php @endphp
            <div class="row-span-6 flex overflow-auto flex-row rounded bg-slate-300 bg-opacity-25 flex-wrap place-content-start items-start px-10 py-5" >
            @foreach ($instance->images as $image)
                <div class="flex justify-center items-center w-1/3 p-3" x-data="{
                    like : {{ $image->favorite ? 'true' : 'false' }},
                    updateFavorite(){ 
                        this.like=!this.like;
                        $wire.emit('updateFavorite',@json($image->id)); 
                    }
                }">
                    <div class="rounded bg-cover bg-center h-80 w-full relative" style="background-image: url({{ url($image->path) }})">
                        <a @click="updateFavorite" class="cursor-pointer absolute rounded-full p-2 drop-shadow-md right-0 mr-3 mt-3" x-bind:class="like? 'bg-blue-500':'bg-slate-500'" >
                            <x-like class="w-8 h-8 " x-bind:class="like ? '' : 'hidden'"></x-like>
                            <x-dislike  class="w-8 h-8 " x-bind:class="like ? 'hidden' : ''"></x-dislike>
                        </a>
                        <div class="flex w-full absolute h-20 bg-white bg-opacity-5 space-x-2 place-content-center justify-center bottom-0 py-5 px-8 items-center">
                            <a wire:click="manage({{ $image->id }})" class="rounded-full  text-center py-1 px-4  w-48 bg-white cursor-pointer ">Manage</a>
                            <a wire:click="delete({{ $image->id }})" class="cursor-pointer rounded-full text-center py-1 px-4 w-48 bg-red-400 text-white">Delete</a>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        @endif
        <!-- Third row -->
        <div class="flex py-4 items-center">
            <input type="text" wire:model="imageTitle" class="rounded bg-slate-600 border-slate-600 focus:border-slate-300 placeholder:text-slate-300 px-4 py-2 bg-slate-400 text-white focus:outline-none focus:ring-slate-600 mr-3 w-128" placeholder="Text">
            <input type="file" wire:model="imageFile" id="imageFile" class="hidden">
            <label for="imageFile" class="cursor-pointer uppercase bg-blue-500 rounded-full px-4 py-2 text-white text-sm w-48 text-center mr-3">Upload</label>
            @error('imageFile') <span class="error text-red-400 text-sm">{{ $message }}</span> @enderror
            @error('imageTitle') <span class="error text-red-400 text-sm">{{ $message }}</span> @enderror
        </div>
        <!-- Popup -->
        <div class="fixed top-0 left-0 right-0 z-50 p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full h-128 justify-center items-center flex bg-black/75" x-show="$wire.managing" >
            <div class="relative max-w-md max-h-full h-1/2 w-2/3" @click.outside="$wire.managing = false">
                <div class="relative  rounded-lg shadow bg-gray-700 bg-opacity-75 py-10 px-14 max-w-full w-90">
                    <button class="text-3xl text-white absolute top-0 right-0 mr-5 mt-5" @click="$wire.managing = false">&times;</button>
                    <h2 class="text-white mb-7">Image: {{ $managingImage->id }}</h2>
                    <form wire:submit.prevent="manageImage">
                        <div class="flex flex-row justify-between mb-6 items-center">
                            <label class="text-white">Alt</label>
                            <input type="text" wire:model="managingImage.alt" class="rounded bg-gray-600 border-gray-600 focus:border-gray-300 placeholder:text-gray-300 w-64 px-4 py-2 bg-gray-400 text-white focus:outline-none focus:ring-gray-600"  placeholder="alt title">
                        </div>
                        <div  class="flex flex-row justify-between mb-8 items-center">
                            <label class="text-white">Name</label>
                            <input type="text" wire:model="managingImage.name" class="rounded bg-gray-600 border-gray-600 focus:border-gray-300 placeholder:text-gray-300 w-64 px-4 py-2 bg-gray-400 text-white focus:outline-none focus:ring-gray-600"  placeholder="name">
                        </div>

                        <div class="flex justify-content-start flex-row items-center items-center">
                            <button class="uppercase bg-blue-500 rounded-full px-4 py-2 text-white text-sm w-36 mr-3" type="submit">Save</button><p class="error text-red-400 text-sm">
                            @error('managingImage.alt') {{ $message }} @enderror
                            @error('managingImage.name'){{ $message }} @enderror </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

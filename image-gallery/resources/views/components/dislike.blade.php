<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 102.28 87.07" {{ $attributes }}>
  <defs>
    <style>
      .cls-2 {
        fill: none;
        stroke: #fff;
        stroke-linecap: round;
        stroke-linejoin: round;
        stroke-width: 3px;
      }
    </style>
  </defs>
  <g id="Layer_1-2" data-name="Layer 1">
    <path class="cls-2" d="m51.14,25.98c0-13.52,11.64-24.48,25.16-24.48s24.48,10.96,24.48,24.48c0,6.76-4.18,13.71-7.17,17.31-9.56,11.53-42.47,42.28-42.47,42.28,0,0-32.63-30.6-42.2-42.01-3.07-3.66-7.45-10.68-7.45-17.59C1.5,12.46,12.46,1.5,25.98,1.5s25.16,10.95,25.16,24.48Z"/>
  </g>
</svg>
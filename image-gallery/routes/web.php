<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class,'index'])->name('home');

Route::get('/posts/{post}',      [App\Http\Controllers\PostController::class,'gallery'])->name('posts.gallery');
Route::get('/cars/{car}',       [App\Http\Controllers\CarController::class,'gallery'])->name('cars.gallery');
Route::get('/authors/{author}',    [App\Http\Controllers\AuthorController::class,'gallery'])->name('authors.gallery');
Route::get('/locations/{location}',  [App\Http\Controllers\LocationController::class,'gallery'])->name('locations.gallery');


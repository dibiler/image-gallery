<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;

class PostController extends Controller
{
    /**
     * Display gallery of the post.
     */
    public function gallery(Post $post)
    {
        return view('pages.post-gallery', compact('post'));
    }
}

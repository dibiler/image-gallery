<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Location;
use App\Models\Author;
use App\Models\Car;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $elements = [];
        $elements['Cars'] = Car::all();
        $elements['Posts'] = Post::all();
        $elements['Authors'] = Author::all();
        $elements['Locations'] = Location::all();
        $models = collect(['Cars','Posts','Authors','Locations']);
        $active = $request->input('group','cars');
        return view('pages/home', compact('elements','models','active'));
    }
}
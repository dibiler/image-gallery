<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Http\Requests\StoreAuthorRequest;
use App\Http\Requests\UpdateAuthorRequest;

class AuthorController extends Controller
{
    /**
     * Display gallery of the author.
     */
    public function gallery(Author $author)
    {
        return view('pages.author-gallery', compact('author'));
    }
}

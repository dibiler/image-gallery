<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Http\Requests\StoreLocationRequest;
use App\Http\Requests\UpdateLocationRequest;

class LocationController extends Controller
{
    /**
     * Display a gallery of the location.
     */
    public function gallery(Location $location)
    {
        return view('pages.location-gallery', compact('location'));
    }
}

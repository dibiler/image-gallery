<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Http\Requests\StoreCarRequest;
use App\Http\Requests\UpdateCarRequest;

class CarController extends Controller
{
    /**
     * Display a gallery of the car.
     */
    public function gallery(Car $car)
    {
        return view('pages.car-gallery', compact('car'));
    }

    
}

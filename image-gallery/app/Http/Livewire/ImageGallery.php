<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Image;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class ImageGallery extends Component
{
    use WithFileUploads;

    public $instance;
    public $instanceName;
    public $pluralInstanceName;
    public $imageFile;
    public $imageTitle;
    public $customMessage;
    public $managing = false;
    public Image $managingImage;

    protected $listeners = ['refreshComponent' => '$refresh','updateFavorite'];

    protected $rules = [
        'managingImage.alt' => 'sometimes|string|max:128',
        'managingImage.name' => 'sometimes|string|max:128',
    ];


    public function updateFavorite(Image $image)
    {
        $image->update(['favorite' => !$image->favorite]);
    }
    
    public function delete(Image $image)
    {   
        $path = str_replace('storage/','public/',$image->path);
        if (Storage::exists($path)) {
            Storage::delete($path);
        }
        $image->delete();
        $this->emit('refreshComponent');
    }

    public function manage(Image $image)
    {
        $this->managingImage = $image;
        $this->managing = true;
        $this->emit('refreshComponent');
    }

    public function manageImage()
    {
        $this->validate([
            'managingImage.alt' => 'required|string|max:255',
            'managingImage.name' => 'required|string|max:255',
        ]);

        if ($this->managingImage->id){
            $this->managingImage->save();
            $this->managingImage = new Image();
            $this->managing = false;
            $this->customMessage = 'Image updated successfully!';
            $this->emit('refreshComponent');
        }
    }

    public function updatedImageFile()
    {
        $this->validate([
            'imageFile' => 'image|max:2048',
            'imageTitle' => 'required|string|max:128',
        ]);

        $this->instance->images()->create([
            'name' => $this->imageTitle,
            'path' => str_replace('public/','storage/',$this->imageFile->store('public/images')),
        ]);

        $this->imageFile = null;
        $this->imageTitle = null;
        $this->customMessage = 'Image uploaded successfully!';
        $this->emit('refreshComponent');
    }
    
    public function mount($instance)
    {
        $this->instance = $instance;
        $this->instanceName = str(get_class($instance))->explode('\\')->last();
        $this->pluralInstanceName = str($this->instanceName)->plural();
        $this->managingImage = new Image();
    }
    public function render()
    {  
        return view('livewire.image-gallery');
    }
}

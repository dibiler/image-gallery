<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        
         \App\Models\Car::factory(20)->create();
         \App\Models\Author::factory(5)->create();
         \App\Models\Post::factory(50)->create();
         \App\Models\Location::factory(2)->create();
         
         foreach (\App\Models\Car::all() as $car) {
             $car->images()->createMany(\App\Models\Image::factory(5)->make()->toArray());
         }
        foreach (\App\Models\Author::all() as $author) {
            $author->images()->createMany(\App\Models\Image::factory(5)->make()->toArray());
        }
        foreach (\App\Models\Post::all() as $post) {
            $post->images()->createMany(\App\Models\Image::factory(5)->make()->toArray());
        }
        
    }
}

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Image>
 */
class ImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {   
        $type = fake()->randomElement(['App\Models\Car', 'App\Models\Post', 'App\Models\Author', 'App\Models\Location']);
        return [
            'name' => fake()->name(),
            'alt' => fake()->sentence(),
            'path' => fake()->imageUrl(),
            'favorite' => fake()->boolean(),
            'imageable_type' => $type,
            'imageable_id' => $type::all()->random()->id,

        ];
    }
}

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'brand' => fake()->randomElement(['Renault', 'Citröen', 'Nissan', 'Volkswagen', 'Peugeot']),
            'model' => fake()->randomElement(['Kangoo', 'Clio', 'Megane', 'C3', 'C4', 'C5', 'NV200', 'Micra', 'Golf', 'Caddy', 'Passat', 'Partner', '208', '308']),
        ];
    }
}

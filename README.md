
## Installation Instructions

- Do the following tasks:
- Go in terminal to the root of the project
- Run: "composer install"
- Run: "npm install"
- Run: "npm run dev"
- On a different terminal:
- Replace name of ".env.test" to ".env"
- Run: "php artisan storage:link"
- Run: "php artisan migrate --seed" (select yes to create the database)
- Run: "php artisan serve"

After these tasks have been done, the application should be running at "localhost:8000"